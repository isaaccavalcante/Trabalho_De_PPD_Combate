
package trabalhodeppdservidor;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.*;
import java.util.ArrayList;
import java.util.Scanner;

public class TrabalhoDePPDServidor {
        
    public static void main(String[] args) {
        
        ArrayList<PrintStream> clientes = new ArrayList<>();
        PrintStream player1 = null;
        PrintStream player2 = null;
        Mensagem mensageFromPlayer1ToPlayer2 = null;
        Mensagem mensageFromPlayer2ToPlayer1 = null;
//        ArrayList<PrintStream> players = new ArrayList<>();
        
        try{
            Scanner input = new Scanner(System.in);
            String linha = "5000"; 
            
            
            System.out.println("Digite a porta para execução do serviço");
            linha = input.nextLine();
            System.out.println(linha);
            
            ServerSocket server = new ServerSocket(Integer.parseInt(linha.toString()));
            Socket socket;
            
            
           
            while(true){
                if(player1 == null){
                    socket = server.accept();
                    player1 = new PrintStream(socket.getOutputStream());
                    mensageFromPlayer1ToPlayer2 = new Mensagem(socket);
                }else if(player2 == null){
                    socket = server.accept();
                    player2 = new PrintStream(socket.getOutputStream());
                    mensageFromPlayer1ToPlayer2.setPlayerToSend(player2);
                    mensageFromPlayer2ToPlayer1 = new Mensagem(socket);
                    mensageFromPlayer2ToPlayer1.setPlayerToSend(player1);
                }  
            }
            
        }catch (IOException e){
            e.printStackTrace();
        }
    }
    
}
