package trabalhodeppdservidor;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;
import java.util.ArrayList;

public class Combate {

    private String gamePatern = "!!!@@@###$$$!@#$";
    private Socket socket;
    private ArrayList<PrintStream> players;

    public Combate(Socket socket, ArrayList<PrintStream> players) {
        this.socket = socket;
        this.players = players;

        Thread();
    }

    private void Thread() {
        Thread t = new Thread(new Runnable() {

            @Override
            public void run() {
                String comand = "";

                try {
                    InputStreamReader inputSR = new InputStreamReader(socket.getInputStream());
                    BufferedReader bufReader = new BufferedReader(inputSR);

                    while ((comand = bufReader.readLine()) != null) {
                        if (comand.length() > 16 && comand.substring(0, 16).equals(gamePatern)) {
                            sendComand(comand);
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        });

        t.start();
    }

    private void sendComand(String message) {
        for (int i = 0; i < players.size(); i++) {
            System.out.println("================ENVIADO PELO COMBATE================");
            System.out.println(message);
            System.out.println("================ENVIADO PELO COMBATE================");
            players.get(i).println(message);
            players.get(i).flush();
        }
    }
}
