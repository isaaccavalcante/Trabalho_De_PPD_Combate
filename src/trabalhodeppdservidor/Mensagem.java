package trabalhodeppdservidor;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.*;
import java.util.ArrayList;
import static javax.swing.JOptionPane.ERROR_MESSAGE;
import static javax.swing.JOptionPane.showMessageDialog;

public class Mensagem {

    private Socket socket;
    private PrintStream playerToSend;

    public Mensagem(Socket socket) {
        this.socket = socket;

        Thread();
    }
    
    public void setPlayerToSend(PrintStream playerToSend){
        this.playerToSend = playerToSend;
    }

    private void Thread() {
        Thread t = new Thread(new Runnable() {

            String message;

            @Override
            public void run() {
                String message = "";

                try {
                    InputStreamReader inputSR = new InputStreamReader(socket.getInputStream());
                    BufferedReader bufReader = new BufferedReader(inputSR);

                    while ((message = bufReader.readLine()) != null) {
                        sendMessage(message);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        });

        t.start();
    }

    private void sendMessage(String message) {
        if(playerToSend != null){
            playerToSend.println(message);
            playerToSend.flush();
        }
    }
}
